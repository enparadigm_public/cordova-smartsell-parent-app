/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
        document.getElementById("init").addEventListener("click", init);
        document.getElementById("validate_user").addEventListener("click", validateUser);
        document.getElementById("open_directory").addEventListener("click", openDirectory);
        document.getElementById("open_potd").addEventListener("click", openPOTD);

        console.log('Received Event: ' + id);
    }
};

app.initialize();



function init() {
    document.getElementById("result_init").innerHTML = " Init Clicked";
    var plugin = new SmartSellPlugin();

    // TODO : Update API_URL_FOR_PLUGIN with actual URL
    plugin.initialize(API_URL_FOR_PLUGIN,function (msg) {
        isInitialized = true;
        document.getElementById("result_init").innerHTML = " Init Success";
        setTimeout(
            function () {
                document.getElementById("result_init").innerHTML = "";
            }, 5000);
    },
        function (err) {
            isInitialized = false;
            document.getElementById("result_init").innerHTML = " Init Failed";
        });
}

function validateUser() {
    document.getElementById("result_validate_user").innerHTML = " Validate User Clicked";
    var plugin = new SmartSellPlugin();
    plugin.validateUser(function (msg) {
        document.getElementById("result_validate_user").innerHTML = " Validate User Success";
        setTimeout(
            function () {
                document.getElementById("result_validate_user").innerHTML = "";
            }, 5000);
    },
        function (err) {
            document.getElementById("result_validate_user").innerHTML = " Validate User Failed";
            console.log(err);
        });
}

function openDirectory() {
    var plugin = new SmartSellPlugin();
    plugin.openDirectory(function (msg) {
        console.log(msg);
    },
        function (err) {
            console.log(err);
        });
}



function openPOTD() {
    var plugin = new SmartSellPlugin();
    plugin.openPOTD(function (msg) {
        console.log(msg);
    },
        function (err) {
            console.log(err);
        });
}
