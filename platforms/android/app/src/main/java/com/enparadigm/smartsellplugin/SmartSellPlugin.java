package com.enparadigm.smartsellplugin;

import android.widget.Toast;

import com.enparadigm.smartsell.LoginTask;
import com.enparadigm.smartsell.SmartSell;
import com.enparadigm.smartsell.SmartSellConfig;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * This class echoes a string called from JavaScript.
 */
public class SmartSellPlugin extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("initialize") && args.getString(0) != null) {
            initSmartSell(args.getString(0), callbackContext);
            return true;
        } else if (action.equalsIgnoreCase("validateUser")) {
            validateUser(callbackContext);
            Toast.makeText(cordova.getContext(), "validateUser", Toast.LENGTH_SHORT).show();
            return true;
        } else if (action.equalsIgnoreCase("openDirectory")) {
            openDirectory(callbackContext);
            Toast.makeText(cordova.getContext(), "openDirectory", Toast.LENGTH_SHORT).show();
            return true;
        } else if (action.equalsIgnoreCase("openPOTD")) {
            openPOTD(callbackContext);
            Toast.makeText(cordova.getContext(), "openPOTD", Toast.LENGTH_SHORT).show();
            return true;
        } else if (action.equals("startActivity")) {
            Toast.makeText(cordova.getContext(), "startActivity", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private void initSmartSell(String baseUrl, CallbackContext callbackContext) {
        try {
            SmartSellConfig config = new SmartSellConfig(baseUrl);
            config.setSignatureFromUserData("Smart Sell", "Developer", "smartsell@enparadigm.com", "9901107631");
            // config.setLanguage("ta");
            SmartSell.init(cordova.getActivity().getApplication(), config);
            callbackContext.success("Success");
            Toast.makeText(cordova.getContext(), "initialize Success", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            if (e.getMessage().equalsIgnoreCase("Koin is already started. Run startKoin only once or use loadKoinModules")) {
                callbackContext.success("Success");
                Toast.makeText(cordova.getContext(), "initialize Success", Toast.LENGTH_SHORT).show();
            } else {
                callbackContext.error("Failed");
                Toast.makeText(cordova.getContext(), "initialize Failed", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void validateUser(CallbackContext callbackContext) {
        SmartSell.validateUser(cordova.getActivity(),  "9901107631", "1", new LoginTask() {
            @Override
            public void success() {
                Toast.makeText(cordova.getActivity(), "Success", Toast.LENGTH_SHORT).show();
                callbackContext.success("Success");
            }

            @Override
            public void failure(Throwable error) {
                Toast.makeText(cordova.getActivity(), "Failure", Toast.LENGTH_SHORT).show();
                callbackContext.error("Failed");
            }
        });
    }

    private void openDirectory(CallbackContext callbackContext) {
        SmartSell.openDirectories(cordova.getActivity());
        callbackContext.success("Success");
    }

    private void openPOTD(CallbackContext callbackContext) {
        SmartSell.openPosterOfTheDay(cordova.getActivity());
        callbackContext.success("Success");
    }
}