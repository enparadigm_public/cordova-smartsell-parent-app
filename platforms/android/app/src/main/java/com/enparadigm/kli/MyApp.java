package com.enparadigm.kli;

import android.app.Application;

import com.enparadigm.smartsell.SmartSell;
import com.enparadigm.smartsell.SmartSellConfig;

public class MyApp extends Application {

    @Override
    public void onCreate() {
        SmartSellConfig config = new SmartSellConfig("http://dev.enparadigm.com/starhealth_dev_api/public/index.php/v1/");
        config.setSignatureFromUserData("Smart Sell", "Developer", "smartsell@enparadigm.com", "9901107631");
//        config.setLanguage("ta");
        SmartSell.init(this, config);
        super.onCreate();
    }
}
