cordova.define('cordova/plugin_list', function(require, exports, module) {
  module.exports = [
    {
      "id": "com.enparadigm.smartsellplugin.SmartSellPlugin",
      "file": "plugins/com.enparadigm.smartsellplugin/www/SmartSellPlugin.js",
      "pluginId": "com.enparadigm.smartsellplugin",
      "clobbers": [
        "SmartSellPlugin"
      ]
    }
  ];
  module.exports.metadata = {
    "cordova-plugin-whitelist": "1.3.4",
    "com.enparadigm.smartsellplugin": "1.0.0"
  };
});