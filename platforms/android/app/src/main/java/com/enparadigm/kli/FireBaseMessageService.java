package com.enparadigm.kli;

import com.enparadigm.smartsell.SmartSell;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FireBaseMessageService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        SmartSell.handleFcmNotification(getApplicationContext(), remoteMessage);
    }
}
