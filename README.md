
# Smartsell Demo Plugin

### Execution

Clone the source code and run the project using ```cordova run android```. In case if you get any error run ``` npm install``` then try running  ```cordova run android```.

In the **index.js** file update ```API_URL_FOR_PLUGIN``` with the URL given by us. Otherwise you get following error.

```
"Uncaught ReferenceError: API_URL_FOR_PLUGIN is not defined", source: file:///android_asset/www/js/index.js (54)
```